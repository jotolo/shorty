require File.expand_path '../spec_helper.rb', __FILE__

describe 'Shorty API' do
  context 'API' do

    context 'GET :shortcode' do
      context 'shortcode does not exist' do
        it 'will return 404 if shortcode does not exist' do
          get '/helloworld'
          expect(last_response.status).to eq(404)
        end

        it 'will redirect to url if shortcode exists' do
          shorten = FactoryBot.create(:shorten, shortcode: 'vwxyz')
          get '/vwxyz'
          expect(last_response.redirect?).to be_truthy
          expect(last_response.body).to include(shorten.url)
        end
      end

      context 'last seen' do
        it 'will update last_seen' do
          shorten = FactoryBot.create(:shorten, shortcode: 'vwxyz')
          expect(shorten.last_seen_date).to_not be_present

          get '/vwxyz'
          shorten = shorten.reload
          expect((shorten.last_seen_date).to_date).to eq(Date.today)

          get '/vwxyz/stats'
          expect((JSON.parse(last_response.body))['lastSeenDate'].to_date).to eq(Date.today)
        end
      end

      context 'redirect_count' do
        it 'will increment redirect_count' do
          shorten = FactoryBot.create(:shorten, shortcode: 'vwxyz')
          expect(shorten.redirect_count).to eq(0)

          get '/vwxyz'
          shorten = shorten.reload
          expect((shorten.redirect_count)).to eq(1)
        end
      end
    end

    context 'GET :shortcode/stats' do
      it 'should show shorten stats' do
        shorten = FactoryBot.create(:shorten)

        get "/#{shorten.shortcode}/stats"
        json = JSON.parse(last_response.body)
        expect(json['startDate'].to_date).to eq(shorten.start_date.to_date)
        expect(json['lastSeenDate']).to be_nil
        expect(json['redirectCount']).to be_nil
      end
    end

    context 'POST creating a new shorten' do
      it 'with url empty returns 400' do
        post 'shorten', url: ''
        expect(last_response.status).to eq(400)
        expect(last_response.body).to include('Url is not present')
      end

      it 'that already exists fails with appropriate error' do
        shorten = FactoryBot.create(:shorten, shortcode: 'vwxyz')
        post 'shorten', shortcode: shorten.shortcode, url: 'http://www.anotherurl.com'
        expect(last_response.status).to eq(409)
        expect(last_response.body).to include('The desired shortcode is already in use. Shortcodes are case-sensitive.')
      end

      it 'with bad shortcode format fails with appropriate error' do
        post 'shorten', shortcode: 'xyz', url: 'http://www.anotherurl.com'
        expect(last_response.status).to eq(422)
        expect(last_response.body).to include('The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$')
      end

      it 'should succeed and return shortcode' do
        post 'shorten', shortcode: 'vwxyz', url: 'http://www.anotherurl.com'
        expect(last_response.status).to eq(201)
        expect(JSON.parse(last_response.body)['shortcode']).to eq('vwxyz')
      end
    end

    context 'POST creating shorten without shortcode' do
      it 'will create new shorten and generate a new token if one was not given' do
        post 'shorten', url: 'http://www.wheredoyouwanttogo.com'
        expect(last_response.status).to eq(201)
        shortcode = (JSON.parse(last_response.body))['shortcode']
        expect(shortcode.length).to eq(6)
      end
    end

  end
end