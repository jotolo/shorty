require 'faker'
FactoryBot.define do
  factory :shorten do
    url { Faker::Internet.url }
    shortcode {
      alphabet = [*'0'..'9',*'A'..'Z',*'a'..'z', '_']
      code = alphabet.sample(6).join while code.nil? || Shorten.exists?(shortcode: code)
      code
    }
  end
end