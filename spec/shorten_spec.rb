require File.expand_path '../spec_helper.rb', __FILE__

describe 'Shorten Model' do
  context 'Model' do

    context 'valid model' do
      it 'should be valid if provided url and valid shortcode' do
        shorten = Shorten.new(url: 'http://www.anotherurl.com', shortcode: 'xxxxxxxxxxyyyyyyyyxxxxxxxzzzz')
        shorten.validate
        expect(shorten.valid?).to be_truthy
      end

      it 'should generate valid shortcode of 6 characters if shortcode was not given' do
        shorten = Shorten.create(url: 'http://www.anotherurl.com')
        expect(shorten.shortcode.length).to eq(6)
      end

      it 'should not change shortcode if it was given' do
        shorten = Shorten.new(url: 'http://www.anotherurl.com', shortcode: 'xxxxxxxxxxyyyyyyyyxxxxxxxzzzz')
        shorten.validate
        expect(shorten.shortcode).to eq('xxxxxxxxxxyyyyyyyyxxxxxxxzzzz')
      end
    end

    context 'invalid model' do
      it 'invalid if shortcode too short' do
        shorten = Shorten.new(url: 'http://www.anotherurl.com', shortcode: 'xxx')
        shorten.validate
        expect(shorten.valid?).to be_falsey
        expect(shorten.errors.keys).to include(:shortcode_not_match_regexp)
      end

      it 'invalid if shortcode fails to match regexp' do
        shorten = Shorten.new(url: 'http://www.anotherurl.com', shortcode: 'xy-0')
        shorten.validate
        expect(shorten.valid?).to be_falsey
        expect(shorten.errors.keys).to include(:shortcode_not_match_regexp)
      end

      it 'invalid if url is missing' do
        shorten = Shorten.new(url: '', shortcode: 'xyza')
        shorten.validate
        expect(shorten.valid?).to be_falsey
        expect(shorten.errors.keys).to include(:missing_url)
      end

      it 'invalid if shortcode already exists' do
        primary_shorten = Shorten.create(url: 'http://www.anotherurl.com')
        shorten = Shorten.new(url: 'http://www.justanotherurl.com', shortcode: primary_shorten.shortcode)
        shorten.validate
        expect(shorten.valid?).to be_falsey
        expect(shorten.errors.keys).to include(:shortcode_already_taken)
      end
    end
  end
end