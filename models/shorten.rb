class Shorten < ActiveRecord::Base
  before_validation :set_shortcode

  validate :url, on: :create do
    self.errors.add(:missing_url, 'Url is not present') if self.url.blank?
  end

  validate :shortcode, on: :create do
    self.errors.add(:shortcode_already_taken, 'The desired shortcode is already in use. Shortcodes are case-sensitive.') if Shorten.exists?(shortcode: self.shortcode)
    self.errors.add(:shortcode_not_match_regexp, 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$') if !(self.shortcode.match(/\A[0-9a-zA-Z_]{4,}\Z/))
  end


  before_create :set_start_date

  def contains_error?(key)
    self.errors.keys.include?(key)
  end

  private

  def self.generate_shortcode
    alphabet = [*'0'..'9',*'A'..'Z',*'a'..'z', '_']
    code = alphabet.sample(6).join while code.nil? || Shorten.exists?(shortcode: code)
    code
  end

  def set_shortcode
    self.shortcode = self.shortcode || Shorten.generate_shortcode
  end

  def set_start_date
    self.start_date =  DateTime.now
  end
end