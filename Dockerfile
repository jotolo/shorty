FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir -p /shorty
WORKDIR /shorty
ENV APP_DIR /shorty

ADD Gemfile $APP_DIR/Gemfile
ADD Gemfile.lock $APP_DIR/Gemfile.lock
ADD . $APP_DIR

RUN gem install bundler
RUN bundle check || bundle install -j 4

# Make port 3000 available to the world outside this container
EXPOSE 3000

CMD ["bash", "-c", "rake db:create; rake db:migrate; rackup -p 3000 -o 0.0.0.0"]