class CreateShortens < ActiveRecord::Migration[5.2]
  def change
    create_table :shortens do |t|
      t.string :url
      t.string :shortcode
      t.integer :redirect_count, default: 0
      t.datetime :start_date
      t.datetime :last_seen_date
      t.timestamps
    end
  end
end
