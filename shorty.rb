require 'sinatra'
require 'sinatra/base'
require 'sinatra/activerecord'
require './models/shorten'
require './workers/shorten_updater_worker'
require 'redis'
require 'sidekiq'
require 'byebug'

class ShortyAPI < Sinatra::Base
  before do
    content_type 'application/json'
  end

  get '/:shortcode' do
    @shorten = Shorten.find_by_shortcode(params[:shortcode])
    if @shorten
      enqueue_job(@shorten.id, DateTime.now)
      status 302
      @shorten.url
    else
      halt(404,'The shortcode cannot be found in the system.')
    end
  end

  get '/:shortcode/stats' do
    @shorten = Shorten.find_by_shortcode(params[:shortcode])
    if @shorten
      response = {
          startDate: @shorten.start_date,
          lastSeenDate: @shorten.last_seen_date
      }
      response[:redirectCount] = @shorten.redirect_count if @shorten.redirect_count != 0
      response.to_json
    else
      halt(404,'The shortcode cannot be found in the system.')
    end
  end

  post '/shorten' do
    @shorten = Shorten.new(url: params[:url], shortcode: params[:shortcode])

    if @shorten.save
      status 201
      @shorten.to_json(only: :shortcode)
    else
      if @shorten.contains_error?(:missing_url)
        halt(400, @shorten.errors[:missing_url])
      elsif @shorten.contains_error?(:shortcode_already_taken)
        halt(409, @shorten.errors[:shortcode_already_taken])
      elsif @shorten.contains_error?(:shortcode_not_match_regexp)
        halt(422, @shorten.errors[:shortcode_not_match_regexp])
      end
    end
  end


  def enqueue_job(shorten_id, last_seen_date)
    if ENV['RACK_ENV'] == 'test'
      suw = ShortenUpdaterWorker.new
      suw.perform(shorten_id, last_seen_date)
    else
      ShortenUpdaterWorker.perform_async(shorten_id, last_seen_date)
    end
  end
end
