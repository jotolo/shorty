require 'sidekiq'
class ShortenUpdaterWorker
  include Sidekiq::Worker
  def perform(shorten_id, seen_date)
    shorten = Shorten.find(shorten_id)
    shorten.redirect_count = shorten.redirect_count + 1
    # Verification needed because the jobs are executed async and with no order
    shorten.last_seen_date = seen_date if shorten.last_seen_date.nil? || shorten.last_seen_date < seen_date
    shorten.save
  end
end